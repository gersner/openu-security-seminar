#!/bin/bash
set -e
source ./env.sh

openuss_build_if_missing
openuss_run -noports python3 attacks/bypass.py