#!/bin/bash
set -e
source ./env.sh

openuss_build_if_missing
openuss_run -noports 'mongo $OPENUSS_HOST_IP:9001/openuss'