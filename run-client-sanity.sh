#!/bin/bash
set -e
source ./env.sh

openuss_build_if_missing
openuss_run -noports -link=openu-server ./build/release/openusec