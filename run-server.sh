#!/bin/bash
set -e
source ./env.sh

openuss_build_if_missing
openuss_run -name=openu-server python3 -m server.runner