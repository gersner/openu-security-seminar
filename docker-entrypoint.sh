#!/bin/bash
echo "OPENU Security Seminar Environment"

export OPENUSS_HOST_IP=$(ip route | awk '/default/{print $3}')

bash -c "$*"