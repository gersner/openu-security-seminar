import os
import ssl
import logging

from .server import Server

OPENUSS_SERVER_PORT = 9000

if "__main__" == __name__:
    logging.basicConfig(level=logging.INFO)

    ROOT_PATH = os.path.join(os.path.dirname(__file__), "..")

    # Build ssl context
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain(
        keyfile=os.path.join(ROOT_PATH, "certs", "server.key"),
        certfile=os.path.join(ROOT_PATH, "certs", "server.crt")
    )

    server = Server(port=OPENUSS_SERVER_PORT, ssl_context=ssl_context)
    server.run()
