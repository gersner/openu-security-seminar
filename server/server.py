import re
import os
import json
import pickle
import struct
import asyncio
import logging
import subprocess
import contextlib

import munch

import motor.motor_asyncio

SERVER_VERSION = "1.3.3.7"

class ClientProtocolException(RuntimeError): pass
class ClientProtocolUnknownCommandException(ClientProtocolException): pass


class ClientProtocol(asyncio.Protocol):
    HEADER_FORMAT = "<LL"
    HEADER_SIZE = struct.calcsize(HEADER_FORMAT)

    def __init__(self, server):
        super().__init__()

        self._server = server
        self._transport = None
        self._user = None
        self._peer_name = None
        self._execution_task = None
        self._buffer = b""

    def connection_made(self, transport):
        self._transport = transport
        self._peer_name = self._transport.get_extra_info("peername")
        self._peer_cert = self._transport.get_extra_info("peercert")

        self._execution_queue = asyncio.Queue()
        self._execution_task = asyncio.ensure_future(self._execution_worker())

        logging.info("Client connected %s", self._peer_name)

    def connection_lost(self, exc):
        logging.info("Client disconnected %s", self._peer_name)
        self._transport = None
        if self._execution_task is not None:
            self._execution_task.cancel()

    def data_received(self, data):
        self._buffer = b"".join([self._buffer, data])

        try:
            while self._process_single_packet():
                pass
        except Exception:
            logging.exception("Failed to process packet. Closing connection")
            self._transport.close()

    def _process_single_packet(self):
        # Check we got the packet size
        if len(self._buffer) < self.HEADER_SIZE:
            return False

        # Check we got entire packet size, this includes the header
        metadata_size, data_size = struct.unpack_from(self.HEADER_FORMAT, self._buffer)
        if len(self._buffer)-self.HEADER_SIZE < metadata_size+data_size:
            return False

        # Slice and parse the individual parts
        packet = self._buffer[self.HEADER_SIZE:self.HEADER_SIZE + metadata_size + data_size]
        self._buffer = self._buffer[self.HEADER_SIZE + metadata_size + data_size:]

        if not metadata_size:
            return True

        # Metadata supports both json and pickle
        metadata_raw = packet[:metadata_size]
        if metadata_raw[0] == 0x80:
            metadata = pickle.loads(metadata_raw)
        else:
            metadata = json.loads(metadata_raw.decode("utf8"))

        if not isinstance(metadata, dict):
            raise ClientProtocolException("Invalid metadata object")

        # Extract data
        data = packet[metadata_size:]

        # Queue command for processing
        logging.info("Server queuing packet %s", metadata)
        asyncio.ensure_future(self._execution_queue.put((metadata, data)))

        return True

    def _process_response(self, response_metadata, response_data):
        # Normalize and validate response
        if response_metadata is None:
            response_metadata = dict()
        elif not isinstance(response_metadata, dict):
            raise ClientProtocolException("Invalid response metadata")

        if response_data is None:
            response_data = b""
        elif not isinstance(response_data, bytes):
            raise ClientProtocolException("Invalid response data")

        # Send request to the queue
        response_metadata_raw = json.dumps(response_metadata).encode("utf8")
        self._transport.write(struct.pack(
            self.HEADER_FORMAT,
            len(response_metadata_raw),
            len(response_data)
        ) + bytes(response_metadata_raw) + response_data)

        logging.info("Sever responding %s", response_metadata)

    async def _execution_worker(self):
        while self._transport is not None:
            # Pop from the queue and run the command
            metadata, data = await self._execution_queue.get()
            try:
                response = await self._execute_command(metadata, data)
                if isinstance(response, dict):
                    response_metadata, response_data = response, None
                elif isinstance(response, bytes):
                    response_metadata, response_data = None, response
                else:
                    response_metadata, response_data = response

                self._process_response(response_metadata, response_data)

            except ClientProtocolUnknownCommandException:
                logging.error("Server received unknown (or incorrect privileges) command %s", metadata)
                self._process_response(None, None)

            except ClientProtocolException:
                logging.error("Server failed processing %s", metadata)
                self._transport.close()

            except:
                logging.exception("Server unexpectedly failed processing %s", metadata)
                self._transport.close()

    async def _execute_command(self, metadata, data):
        logging.info("Server executing packet %s", metadata)

        # User get request commands based in it authentication level
        privs = ["UNPRIVILEGED"]
        if self._user:
            privs.append("AUTHENTICATED")

        if self._user and self._user.admin:
            privs.append("ADMIN")

        # Find the command
        func = None
        command_name = str(metadata.get("command", None))
        for priv in privs:
            func = getattr(self, "_command_{}_{}".format(priv, command_name), None)
            if func:
                break

        if not func:
            raise ClientProtocolUnknownCommandException("Command not found")

        logging.info("Server executing command %s (%s)", metadata, func.__name__)

        if asyncio.iscoroutinefunction(func):
            return await func(metadata, data)
        else:
            return func(metadata, data)

    def _command_UNPRIVILEGED_get_server_info(self, metadata, data):
        return dict(version=SERVER_VERSION)

    async def _command_UNPRIVILEGED_login(self, metadata, data):
        username = metadata.get("username", None)
        password = metadata.get("password", None)

        with self._server.db() as db:
            self._user = munch.munchify(await db.users.find_one(dict(username=username, key=password)))

        if self._user:
            # Ensure user folder
            os.makedirs(os.path.join(self._server.data_folder, username), exist_ok=True)

        return dict(success=self._user is not None)

    def _command_AUTHENTICATED_download_file(self, metadata, data):
        try:
            root_path = os.path.realpath(os.path.join(self._server.data_folder, self._user.username))
            file_path = os.path.realpath(os.path.join(root_path, metadata["path"]))
            if not file_path.startswith(root_path):
                raise ClientProtocolException("Invalid path provided")

            logging.info("Service will fetch file %s", file_path)

            stat = os.stat(file_path)
            data = open(file_path, "rb").read()

            return dict(exists=True, modified=stat.st_mtime, created=stat.st_ctime), data
        except FileNotFoundError:
            return dict(exists=False, modified=None, created=None)

    def _command_AUTHENTICATED_upload_file(self, metadata, data):
        root_path = os.path.realpath(os.path.join(self._server.data_folder, self._user.username))
        file_path = os.path.realpath(os.path.join(root_path, metadata["path"]))
        if not file_path.startswith(root_path):
            raise ClientProtocolException("Invalid path provided")

        logging.info("Service will upload file %s", file_path)

        open(file_path, "wb").write(data)

        return dict(success=True), None

    async def _command_ADMIN_shell(self, metadata, data):
        process = await asyncio.create_subprocess_shell(metadata["shell"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.DEVNULL)
        stdout, _ = await process.communicate()
        return dict(code=process.returncode, success=True), stdout

class Server(object):
    MONGODB_GRACE_TIMEOUT_IN_SECONDS = 7

    def __init__(self, *, port, ssl_context=None, ip="0.0.0.0", data_folder="data"):
        self._ssl_context = ssl_context
        self._port = port
        self._ip = ip
        self._data_folder = data_folder

        self._mongodb = None
        self._server = None


    def run(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._start_serve())

        try:
            loop.run_forever()
        except KeyboardInterrupt:
            loop.run_until_complete(self._stop_serve())

    @contextlib.contextmanager
    def db(self):
        client = motor.motor_asyncio.AsyncIOMotorClient()
        yield client.openuss

    @property
    def data_folder(self):
        return self._data_folder

    async def _start_serve(self):
        logging.info("Server starting")

        # Initialize the database
        self._start_mongodb()

        # Initialize the data
        os.makedirs(self._data_folder, exist_ok=True)

        # Load the initial database
        with self.db() as database:
            for file_name in os.listdir("database"):
                file_match = re.match("^(\w+)\.json$", file_name)
                if not file_match:
                    continue
                collection = database[file_match.group(1)]

                for line in open(os.path.join("database", file_name), "rt").readlines():
                    await collection.insert(json.loads(line))

        # Start serving clients
        self._server = await asyncio.get_event_loop().create_server(lambda: ClientProtocol(self), self._ip, self._port, ssl=self._ssl_context, reuse_address=True)

    async def _stop_serve(self):
        logging.info("Server stopping")
        if self._server is not None:
            self._server.close()
            await self._server.wait_closed()

        self._stop_mongodb()

    def _configure_mongodb_env(self):
        os.mkdir("/run/mongodb", 755)
        os.chdir("/run/mongodb")

    def _start_mongodb(self):
        self._mongodb = subprocess.Popen(["/usr/bin/mongod", "--bind_ip=0.0.0.0", "--unixSocketPrefix=/run/mongodb", "--config=/etc/mongodb.conf"], preexec_fn=self._configure_mongodb_env)

    def _stop_mongodb(self):
        if self._mongodb is not None:
            self._mongodb.kill()
            try:
                self._mongodb.wait(timeout=self.MONGODB_GRACE_TIMEOUT_IN_SECONDS)
            except subprocess.TimeoutExpired:
                self._mongodb.terminate()
