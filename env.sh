export OPENUSS_ROOT_PATH=$(echo $0 | python -c "import os; import sys; sys.stdout.write(os.path.abspath(os.path.dirname(sys.stdin.read())))")
export OPENUSS_IMAGE_NAME=${OPENUSS_IMAGE_NAME:=openuss}

openuss_run() {
    local ports="-p 9000:9000 -p 9001:27017"
    local name="";
    local links="";
    for arg in "$@"
    do
        if [[ "$arg" =~ ^-.* ]]; then
            case "$arg" in
                -noports)
                    ports=""
                    ;;
                -name=*)
                    name="--name ${arg#-name=}"
                    ;;
                -link=*)
                    links="$links --link ${arg#-link=}"
                    ;;

            esac
            shift
        else
            break
        fi
    done

    docker run --rm -ti --privileged $links $name $ports -v $OPENUSS_ROOT_PATH:/workdir $OPENUSS_IMAGE_NAME "$*"
}

openuss_build_if_missing() {
    if ! docker images openuss | grep openuss >/dev/null; then
        echo "Docker image is missing. Building image"
        ./docker-build-image.sh
    fi
}