import os
import ssl
import json
import enum
import pickle
import struct
import socket

SERVER_ADDRESS = os.environ.get("OPENUSS_HOST_IP", "127.0.0.1")
SERVER_PORT = 9000


class SimpleClient(object):
    class SERIALIZATION(enum.Enum):
        JSON = 1
        PICKLE = 2

    HEADER_FORMAT = "<LL"
    HEADER_SIZE = struct.calcsize(HEADER_FORMAT)

    def __init__(self, server_address=None, server_port=None):
        self._server_address = server_address or SERVER_ADDRESS
        self._server_port = server_port or SERVER_PORT

    def connect(self):
        self._socket = socket.socket()
        self._ssl_socket = ssl.wrap_socket(self._socket)
        self._ssl_socket.connect((self._server_address, self._server_port))

    def send(self, metadata, data=None, *, serialization=SERIALIZATION.JSON):
        # Send a single packet
        self._send_packet(metadata, data, serialization=serialization)

        # Receive a single packet
        return self._receive_packet()

    def login(self, username, password):
        return self.send(dict(command="login", username=username, password=password))[0]

    def download_file(self, path):
        return self.send(dict(command="download_file", path=path))[1]

    def get_server_info(self):
        return self.send(dict(command="get_server_info"))[0]

    def shell(self, shell):
        return self.send(dict(command="shell", shell=shell))

    def _send_packet(self, metadata, data=None, *, serialization=SERIALIZATION.JSON):
        data = b"" if not data else bytes(data)

        if serialization is self.SERIALIZATION.JSON:
            metadata_raw = json.dumps(metadata).encode("utf8")
        else:
            metadata_raw = pickle.dumps(metadata)

        self._ssl_socket.sendall(struct.pack(self.HEADER_FORMAT, len(metadata_raw), len(data)) + metadata_raw + data)

    def _receive_packet(self):
        header = self._recv_all(self.HEADER_SIZE)
        if not header:
            return None

        metadata_size, data_size = struct.unpack(self.HEADER_FORMAT, header)
        buffer = self._recv_all(metadata_size + data_size)
        if not buffer:
            return None

        return json.loads(buffer[:metadata_size].decode("utf8")), buffer[metadata_size:]

    def _recv_all(self, size):
        buffer = b""
        while len(buffer) != size:
            chunk = self._ssl_socket.recv(size - len(buffer))
            if not chunk:
                return None

            buffer = b"".join([buffer, chunk])

        return buffer
