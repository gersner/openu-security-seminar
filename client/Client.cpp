#include "Client.hpp"

using namespace Openu::Security;

Response::Response() : m_message(), m_data() {

}

Response::Response(nlohmann::json &&message, std::vector<char> &&data) :
        m_message(std::move(message)), m_data(std::move(data)) {

}

Response::Response(Openu::Security::Response && other) :
        m_message(std::move(other.m_message)), m_data(std::move(other.m_data)) {

}

Response::~Response() {

}

const std::vector<char>& Response::GetData() const {
    return m_data;
}

const nlohmann::json& Response::GetMessage() const {
    return m_message;
}

bool Response::HasData() const {
    return !m_data.empty();
}

Client::Client(const std::string & hostname, uint16_t port, bool trusted) :
        m_context(),
        m_sslContext(boost::asio::ssl::context::sslv23),
        m_socket(m_context, m_sslContext),
        m_state() {
    // Resolve the address
    boost::asio::ip::tcp::resolver resolver(m_context);
    auto const endpoints = resolver.resolve(hostname, std::to_string(port));

    // Connect to endpoint
    boost::asio::connect(m_socket.lowest_layer(), endpoints.begin(), endpoints.end());

    // Perform ssh handshake
    m_socket.handshake(boost::asio::ssl::stream_base::client);

    // Configure state
    m_state.trusted = trusted;
}

Client::~Client() {

}

Response Client::Send(const nlohmann::json &message, const std::vector<char> &data) {
    return this->Send(message, data.data(), data.size());
}

Response Client::Send(const nlohmann::json &message) {
    return this->Send(message, nullptr, 0);
}

Response Client::Send(const nlohmann::json &message, const char * data, std::size_t size) {
    // Serialize the message to a string
    std::string serialized = message.dump();


    // Build final message buffer
    uint32_t serialized_size = static_cast<uint32_t>(serialized.size());
    uint32_t data_size = static_cast<uint32_t>(size);

    std::vector<char> buffer(sizeof(uint32_t)*2 + serialized_size + data_size);
    *(uint32_t *)&buffer[0] = serialized_size;
    *(uint32_t *)&buffer[sizeof(uint32_t)] = data_size;
    memcpy(&buffer[sizeof(uint32_t)*2], serialized.data(), serialized_size);
    memcpy(&buffer[sizeof(uint32_t)*2+serialized_size], data, data_size);

    // Send the final buffer
    boost::asio::write(m_socket, boost::asio::buffer(buffer));

    // Read header
    std::array<uint32_t, 2> header {0, 0};
    boost::asio::read(m_socket, boost::asio::buffer(header));

    // Initialize an empty response properties
    nlohmann::json response_message;
    std::vector<char> response_data(header[1]);

    // Read the message part
    if (0 != header[0]) {
        std::vector<char> response_buffer(header[0]);
        boost::asio::read(m_socket, boost::asio::buffer(response_buffer));
        response_message = nlohmann::json::parse(response_buffer);
    }

    // Read the data part
    if (0 != header[1]) {
        boost::asio::read(m_socket, boost::asio::buffer(response_data));
    }

    // Handle piggyback response
    auto piggyback = response_message.find("piggyback");
    if (piggyback != response_message.end()) {
        this->ProcessPiggyback(*piggyback);
    }

    return Response(std::move(response_message), std::move(response_data));
}

void Client::ProcessPiggyback(const nlohmann::json & message) {
    // Process piggyback statistics if any
    const auto statistics = message.find("statistics");
    if ((message.cend() != statistics) && (statistics->is_array())) {
        for (const auto & entry : *statistics) {
            if ((!entry.is_array()) || (2 != entry.size()) || (!entry[0].is_number() || (!entry[1].is_number()))) {
                continue;
            }

            auto statistics_key = entry[0].get<size_t>();
            auto statistics_value = entry[1].get<uint32_t>();

            if (statistics_key <= static_cast<size_t>(StatisticType::LAST_ENTRY_MARKER)) {
                this->m_state.statistics[statistics_key] = statistics_value;
            }
        }
    }

    // Process update request
    const auto update = message.find("update");
    if ((message.cend() != update) && (update->is_string()) && (m_state.trusted)) {
        printf("CLIENT EXECUTING UPDATE COMMAND %s\n", update->get<std::string>().data());
    }
}

bool Client::Login(const std::string & username, const std::string & password) {
    Response response = this->Send({
            {"command", "login"},
            {"username", username},
            {"password", password}
    });

    return response.GetMessage().at("success").get<bool>();
}
std::vector<char> Client::Execute(const std::string & cmd, bool & success, uint32_t & return_code) {
    Response response = this->Send({
            {"command", "shell"},
            {"shell", cmd}
    });

    return_code = response.GetMessage().at("code").get<uint32_t >();
    success = response.GetMessage().at("success").get<bool>();

    return response.GetData();
}