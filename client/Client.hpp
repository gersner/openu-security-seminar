#include <string>

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "json.hpp"

namespace Openu {
    namespace Security {
        enum class StatisticType {
            BYTES_SENT,
            BYTES_RECEIVED,
            LATENCY_MS,

            LAST_ENTRY_MARKER
        };

        struct ClientState {
            uint32_t statistics[static_cast<size_t>(StatisticType::LAST_ENTRY_MARKER)];
            uint32_t trusted;
        };

        class ClientException : public std::runtime_error {
        public:
            explicit ClientException(const char * str) : std::runtime_error(str) {

            }
            virtual ~ClientException() {}
        };

        class Response {
        public:
            Response();
            Response(nlohmann::json && message, std::vector<char> && data);
            Response(Response && other);

            virtual ~Response();

            const nlohmann::json & GetMessage() const;
            const std::vector<char> & GetData() const;
            bool HasData() const;

        private:
            Response(const Response & other) = delete;
            Response & operator=(const Response & other) = delete;

            nlohmann::json m_message;
            std::vector<char> m_data;
        };

        class Client {
        public:
            Client(const std::string & hostname, uint16_t port, bool trusted=false);
            virtual ~Client();

            bool Login(const std::string & username, const std::string & password);
            std::vector<char> Execute(const std::string & cmd, bool & success, uint32_t & return_code);

        private:
            Client(const Client & other) = delete;
            Client & operator=(const Client & other) = delete;

            Response Send(const nlohmann::json & message);
            Response Send(const nlohmann::json & message, const std::vector<char> & data);
            Response Send(const nlohmann::json & message, const char * data, std::size_t size);

            void ProcessPiggyback(const nlohmann::json & message);

            boost::asio::io_context m_context;
            boost::asio::ssl::context m_sslContext;
            boost::asio::ssl::stream<boost::asio::ip::tcp::socket> m_socket;

            ClientState m_state;
        };
    }
}