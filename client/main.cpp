#include <cstdio>

#include "Client.hpp"

int main() {
    printf("OPENUSEC Initializing client\n");

    try {
        // Initialize an untrusted client
        Openu::Security::Client client("openu-server", 9000, false);

        // Do login
        if (!client.Login("admin", "root-secret-key")) {
            throw std::runtime_error("Failed to login");
        }

        // Run command
        bool success = false;
        uint32_t  error_code = 0;
        std::vector<char> response = client.Execute("/bin/hostname", success, error_code);
        if ((0 == response.size()) || (!success) || (0 != error_code)) {
            throw std::runtime_error("Failed to execute command");
        }

        printf("OPENUSEC hostname=%s\n", response.data());
    } catch (const Openu::Security::ClientException & ex) {
        printf("OPENUSEC Client failure: %s\n", ex.what());
    } catch (const std::exception & ex) {
        printf("OPENUSEC Unexpected failure: %s\n", ex.what());
    }
}