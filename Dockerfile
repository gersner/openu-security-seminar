FROM ubuntu:18.04

RUN DEBIAN_FRONTEND=noninteractive apt update && DEBIAN_FRONTEND=noninteractive apt upgrade -y && DEBIAN_FRONTEND=noninteractive apt install -y \
    python3 python3-pip \
    iproute2 \
    mongodb-server \
    cmake \
    libssl-dev \
    wget

WORKDIR /installdir
ADD requirements.txt /installdir
RUN pip3 install -r requirements.txt

RUN wget https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_67_0.tar.gz && \
    tar xzvf boost_1_67_0.tar.gz && \
    cd boost_1_67_0 && \
    ./bootstrap.sh && \
    ./b2 install --without-python --without-mpi --without-coroutine --without-graph --without-graph_parallel

EXPOSE 9000
EXPOSE 9001

WORKDIR /workdir
ENTRYPOINT ["/workdir/docker-entrypoint.sh"]
CMD ["bash"]