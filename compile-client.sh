#!/bin/bash
set -e
source ./env.sh

openuss_build_if_missing
openuss_run -noports "mkdir -p ./build/release && cd ./build/release && cmake -DBOOST_ROOT=/usr/local/include/boost -DCMAKE_BUILD_TYPE=Release ../../client && make && pwd"